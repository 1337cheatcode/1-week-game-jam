extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int) var speed = 500

var velocity = Vector2()
var state = 0
var collision
var tempox = -256
var tempoy = -150

# Called when the node enters the scene tree for the first time.
func _ready():
	# Replace with function body.
	tempox = global_position.x+256
	tempoy = global_position.y+150

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	pass
	if position.x <= tempox-256 && position.y <= tempoy+150:
		state = 0
	elif position.x <= tempox+256 && position.y >= tempoy+150:
		state = 1
	elif position.x >= tempox+256 && position.y >= tempoy-150:
		state = 2
	elif position.x >= tempox-150 && position.y <= tempoy-150:
		state = 3
	rotation+=(1.5*delta*Dimen.time_scale)

func _physics_process(delta):
	velocity = Vector2()
	match state:
		0:velocity.y += 1
		1:velocity.x += 1
		2:velocity.y -= 1
		3:velocity.x -= 1
	if move_and_collide(velocity.normalized()*speed*Dimen.time_scale*delta):
		get_tree().change_scene("res://Scenes/Main Menu.tscn")