extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var sprites = self.get_node("AnimatedSprite")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Dimen.time_scale == 0:
		sprites.playing = false
	elif sprites.playing == false:
		sprites.playing = true
	elif move_and_collide(Vector2()):
		get_tree().change_scene("res://Scenes/Main Menu.tscn")
