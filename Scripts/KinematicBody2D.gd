extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = 600

const UP = Vector2(0,-1)
const Cooldown = preload("res://Scripts/Cooldown.gd")
const Teleport = preload("res://Scenes/Teleport.tscn")
const MTeleport = preload("res://Scenes/MTeleport.tscn")
const Caststop = preload("res://Scenes/CastStop.tscn")

var velocity = Vector2()
var jump_count = 1

onready var animator = self.get_node("AnimationPlayer")
onready var sprite = self.get_node("Sprite")
onready var skill2cd = Cooldown.new()
onready var skill3cd = Cooldown.new()
#onready var skill3sprite = self.get_node("Sprite2")

# Called when the node enters the scene tree for the first time.
func _ready():
	# Replace with function body.
	skill2cd.max_time = 3.5
	skill3cd.max_time = 0.75
	#skill3sprite.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	skill2cd.tick(delta)
	skill3cd.tick(delta)
	if is_on_floor():
		jump_count=1
	if velocity.x > 0:
		sprite.flip_h = false
	elif velocity.x < 0:
		sprite.flip_h = true
	if Input.is_action_pressed("ui_cancel"):
		Dimen.time_scale = 1
		get_tree().change_scene("res://Scenes/Main Menu.tscn")
	#if skill3sprite.visible == true:
	#	skill3sprite.frame += 1
	#	yield(get_tree().create_timer(0.001),"timeout")
	#if skill3sprite.frame > 81:
	#	skill3sprite.visible = false
	#	skill3sprite.position = Vector2()
	#	skill3sprite.frame = 0

func _input(event):
#	velocity=Vector2()
#	if event is InputEventKey:
#		if event.pressed:
#			if event.scancode == global.custom_main_up:
#				velocity.y-=1
#			if event.scancode == global.custom_main_down:
#				velocity.y+=1
#			if event.scancode == global.custom_main_right:
#				velocity.x+=1
#			if event.scancode == global.custom_main_left:
#				velocity.x-=1
	if event is InputEventKey:
		if event.pressed && !event.echo:
			if event.scancode == global.skill2 && skill2cd.is_ready() && Dimen.time_scale != 0:
				var temp = Caststop.instance()
				temp.global_position = self.global_position + Vector2(0,-50)
				get_parent().add_child(temp)
				Dimen.time_scale = 0
				temp.play()
				yield(get_tree().create_timer(1.5), "timeout")
				Dimen.time_scale = 1
			if event.scancode == global.skill3 && skill3cd.is_ready():
				var temp = Teleport.instance()
				temp.global_position = self.global_position
				get_parent().add_child(temp)
				temp.play()
				if Input.is_key_pressed(global.custom_main_up) || Input.is_key_pressed(global.custom_main_right) || Input.is_key_pressed(global.custom_main_left) || Input.is_key_pressed(global.custom_main_down):
					temp = Vector2()
					if Input.is_key_pressed(global.custom_main_up):
						temp.y-=1
					if Input.is_key_pressed(global.custom_main_right):
						temp.x+=1
					if Input.is_key_pressed(global.custom_main_left):
						temp.x-=1
					if Input.is_key_pressed(global.custom_main_down):
						temp.y+=1
					position+=(temp.normalized()*150)
					#skill3sprite.position-=(temp.normalized()*150)
				elif sprite.flip_h:
					position+=(Vector2(-1,-1).normalized()*150)
					#skill3sprite.position-=(Vector2(-1,-1).normalized()*150)
				else:
					position+=(Vector2(1,-1).normalized()*150)
					#skill3sprite.position-=(Vector2(1,-1).normalized()*150)
				if velocity.y>0:
					velocity.y=0
				#skill3sprite.visible = true
				temp = MTeleport.instance()
				temp.global_position = self.global_position
				get_parent().add_child(temp)
				temp.play()
			if event.scancode == global.custom_main_up:
				if animator.current_animation.match("up"):
					animator.seek(0)
				if is_on_floor():
					velocity.y=-jump_speed
				elif jump_count>0:
					velocity.y=-jump_speed
					jump_count-=1

func get_input():
	velocity.x = 0
	if Input.is_key_pressed(global.custom_main_right):
		velocity.x+=speed
	if Input.is_key_pressed(global.custom_main_left):
		velocity.x-=speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity=move_and_slide(velocity,UP)
	if velocity.x != 0:
		animator.play("walk")
	elif velocity.y < 0:
		animator.play("up")
	elif Input.is_key_pressed(global.custom_main_down):
		animator.play("duck")
	else:
		animator.play("stand")

func ded():
	Dimen.time_scale = 1
	get_tree().change_scene("res://Scenes/Main Menu.tscn")