extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area2D2_body_entered(body):
	on_body_entered(body) # Replace with function body.

func _on_Area2D_body_entered(body):
	on_body_entered(body) # Replace with function body.

func on_body_entered(body):
	if body.get_name().match("KinematicBody2D"):
		Dimen.time_scale = 1
		get_tree().change_scene("res://Scenes/Main Menu.tscn")

func _on_Area2D3_body_entered(body):
	on_body_entered(body) # Replace with function body.

func _on_Area2D4_body_entered(body):
	on_body_entered(body) # Replace with function body.
